User-agent: *
Disallow: /accounts/
Disallow: /admin/
Disallow: /django-admin/
Disallow: /archives/*/vote$
