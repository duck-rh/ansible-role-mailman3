# Ansible role for Mailman 3 installation

## Introduction

[Mailman](http://www.list.org/) is a mailing-list manager. It is a daemon and a WSGI (Python) application with a PostgreSQL database.

Version 3 introduce major installation and configuration changes over version 2, and this role only handles version 3.x.

This role installs and configure the application and the database, but leaves the web server configuration details to your care.
The migration script needed to switch from version 2 is provided.

This work is based on the Fedora infra role:
  http://infrastructure.fedoraproject.org/infra/ansible/roles/mailman/

## Requirements

This role also expects to work hand-in hand with the OSAS httpd role
(`ansible-role-ah-httpd`). See the `mailing-lists-server` role for a
full working example on how to coordinate these roles.

PostgreSQL and memcached servers should be installed and running before using this role (not necessarily on the same machine).

## Variables

- **site_owner_email**: email of the system owner
- **site_admins**: list of site administrators usernames
                   if defined, grant priviledges to these usernames and revoke to all others
                   if undefined, let the current priviledges untouched
- **from_email**: sender email of messages sent by Mailman
- **webui_name**: Title of the web UI
- **webui_basedir**: where the web interface files are installed
- **webui_confdir**: configuration directory of the Mailman UI
- **db_server**: hostname of the database server (if remote)
- **db_name**: Mailman main database name
- **db_user**: Mailman user name for the main database
- **http_vhost**: vhost name; the vhost is not created in this role but configuration for this
                  instance will be added to this vhost
- **hyperkitty_db_name**: Mailman UI database name
- **hyperkitty_db_user**: Mailman user name for the UI database
- **hyperkitty_admin_db_user**: Mailman administrator user name for the UI database
- **domains**: list of web domains managed by Mailman
               it is a hash of domain -> display name
- **multi_domains**: If true (the default), then each vhost will only display the lists associated with it;
                     If False, all vhost will display the same full list of mailing-lists
- **with_postfix**: is Mailman working with Postfix
- **memcached_host**: Memcached IP address (if remote)
- **memcached_port**: Memcached port (if non standard)
- **rest_user**: username for the REST API credentials
- **rest_pw**: password for the REST API credentials
- **auth.{service_name}**: enable social authentication for systems requiring an API account
   each `service_name` (free name) has a `provider` (github, google…), a `display_name` as title in the web UI,
   as well as a `client_id` and `client_secret` to connect to the provider
- **auth_oidc**: AllAuth library does not support OIDC yet, thus using django-auth-oidc plugin
                 *Warning* current OIDC auth integration takes over all other forms of auth
                           (from allauth library as well as local accounts)
    - **server**: server base URL with path to the realm endpoint
                  it needs to end with a / because this fix is not included in the latest Python 2 supported version of openid-connect:
                    https://gitlab.com/aiakos/python-openid-connect/commit/90ff97ad6359548e139a1135ea7ee6f845f2eef3
    - **client_id**
    - **client_secret**
- **use_custom_favicon**: instead of using the Hyperkitty favicon, use a custom file for this site;
                          this file needs to be installed in the custom assets directory, read below

## Customization

To customize the website you can override Hyperkitty's templates by
adding your own into `{{ webui_basedir }}//templates/…`. The exact
subdirectory depends on the application (hyperkitty, django\_mailman3,
account…) and the exact filename on which aspect you wish to
customize. This is tied to the exact version installed, and the
documentation (as of 2019-01-15) is lacking, so you will most probably
need to look at the source code.

These custom templates may need specific assets too, you can install
them using Ansible by adding rules after this role to install them into
`{{ webui_basedir }}/static-extra/`. When you assets change, Django
needs to be notified, this can be done automaically using the
`Update static files` handler of this role.

